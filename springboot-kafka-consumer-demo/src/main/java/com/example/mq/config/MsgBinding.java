package com.example.mq.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface MsgBinding {

    String SEND_MSG = "sendMsg";
    String SEND_EMAIL = "sendEmail";

    String RECEIVER_MSG = "receiverMsg";
    String RECEIVER_EMAIL = "receiverEmail";

    @Output(MsgBinding.SEND_EMAIL)
    MessageChannel sendEmail();

    @Output(MsgBinding.SEND_MSG)
    MessageChannel sendMsg();


    @Output(MsgBinding.RECEIVER_MSG)
    MessageChannel receiverMsg();

    @Output(MsgBinding.RECEIVER_EMAIL)
    MessageChannel receiverEmail();


}
