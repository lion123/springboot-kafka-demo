package com.example.mq.service;

import com.example.mq.KafkaProducerApplicationTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MsgSenderTest extends KafkaProducerApplicationTest {

    @Autowired
    MsgSender sender;

    @Test
    public void send() {
        sender.send("这是Kafka发送的消息内容" + System.currentTimeMillis());
    }
}