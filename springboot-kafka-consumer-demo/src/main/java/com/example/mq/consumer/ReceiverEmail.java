package com.example.mq.consumer;

import com.example.mq.config.MsgBinding;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;

@Slf4j
public class ReceiverEmail {

    @StreamListener(MsgBinding.RECEIVER_EMAIL)
    private void receive(Message<String> message) {
        log.info("receive message : " + message.getPayload());
    }

}
