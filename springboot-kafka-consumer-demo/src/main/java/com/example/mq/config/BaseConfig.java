package com.example.mq.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by liulanhua on 2018/8/31.
 */
@Slf4j
@Configuration
@EnableBinding(value = {MsgBinding.class})
public class BaseConfig extends WebMvcConfigurationSupport {


    @Value("${spring.application.name}")
    private String appName;
    @Value("${eureka.instance.hostname:localhost}")
    private String hostname;
    @Value("${server.port:8080}")
    private int port;


    @Bean(name = "hostName")
    public String getHostName() {
        log.info(">>>>>> 访问 http://{}:{} 可以查看{}服务信息", hostname, port, appName);
        try {
            if (StringUtils.isNotBlank(appName)) {
                appName = InetAddress.getLocalHost().getHostName();
            }
        } catch (UnknownHostException e) {
            log.error(">>>>>> UnknownHostException", e);
        }
        return appName;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars*")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/static*")
                .addResourceLocations("classpath:/META-INF/resources/static/");
    }

    @Bean
    public Globals globals() {
        return new Globals();
    }


}
